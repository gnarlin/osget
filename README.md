# Osget

Downloads ISO files for different operating systems. A package manager for operating systems image files.

# Installation:

git clone git@gitlab.com:gnarlin/osget.git

cd osget

sudo ./install.sh --install

You can also use the debian package on Debian based distributions.

sudo dpkg -i osget-0.6.deb
